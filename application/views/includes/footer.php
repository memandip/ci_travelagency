<hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <h3>
                        <p style="text-align:center;">Copyright &copy; MyTravelAgency</p>
                    </h3>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?=base_url();?>assets/js/jquery.js"></script>

    <script src="<?=base_url();?>assets/js/scripts.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
</body>

</html>